using System;
using System.Collections.Generic;
using System.Text;

namespace KK.EIS.TianYanCha
{
    public class EISOptions
    {
        /// <summary>
        /// 权限
        /// </summary>
        public string Token { get; set; }

        /// <summary>
        /// 明细
        /// </summary>
        public string StudyName{get;set;}
        /// <summary>
        /// 号码
        /// </summary>
        public string StudyNumber{get;set;}
    }
}
