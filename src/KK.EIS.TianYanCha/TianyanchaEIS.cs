﻿using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Text;
using Newtonsoft.Json;
using System.Threading.Tasks;

namespace KK.EIS.TianYanCha
{
    public class TianyanchaEIS : IEISManager
    {
        private readonly EISOptions _options;

        public TianyanchaEIS(IOptions<EISOptions> options)
        {
            _options = options.Value;

           
        }

        public ResultMessage<EnterpriseMessageOutput> EnterpriseBasicInformationQueryAsync(string companyMessage)
        {
            // token可以从 数据中心 -> 我的接口 中获取
            //var token = "";
            var url = "http://open.api.tianyancha.com/services/open/ic/baseinfo/normal?keyword=" + companyMessage;

            // 请求处理
            var responseStr = HttpGetEnterprise(url, _options.Token);
            return responseStr;
        }

        private ResultMessage<EnterpriseMessageOutput> HttpGetEnterprise(string url, string token)
        {

            var result = new ResultMessage<EnterpriseMessageOutput>
            {
                Result = new EnterpriseMessageOutput()
            };

            try
            {
                HttpWebRequest request = (HttpWebRequest)WebRequest.Create(url);

                // set header
                WebHeaderCollection headers = new WebHeaderCollection();
                headers.Add("Authorization", token);
                request.UserAgent = null;
                request.Headers = headers;
                request.Method = "GET";

                // response deal
                HttpWebResponse response = (HttpWebResponse)request.GetResponse();
                var httpStatusCode = (int)response.StatusCode;
                //Logger.Info($"请求接口后返回的状态码：{httpStatusCode}");
                if (httpStatusCode == 200)
                {
                    Stream myResponseStream = response.GetResponseStream();
                    StreamReader myStreamReader = new StreamReader(myResponseStream, Encoding.GetEncoding("utf-8"));
                    string retString = myStreamReader.ReadToEnd();

                    var resultMesg = JsonConvert.DeserializeObject<ResultMessage<EnterpriseMessageOutput>>(retString);
                    if (resultMesg.Error_code > 0)
                    {
                        if (resultMesg.Error_code == 300000)
                        {
                            result.Reason = "通过当前的社会信用代码未获取到企业数据，请核对后重新验真。";
                            //throw new Exception($);
                        }
                        else
                        {
                            result.Error_code = resultMesg.Error_code;
                            result.Reason = resultMesg.Reason;
                            //throw new Exception($"请求接口不成功，状态码：{resultMesg.Error_code}，说明：{resultMesg.Reason}");
                        }

                    }
                    else
                    {
                        result = resultMesg;
                    }
                    myStreamReader.Close();
                    myResponseStream.Close();
                }
                else
                {   // todo 可以通过返回码判断处理
                    //Logger.Info($"请求接口后返回的状态码：{httpStatusCode}");
                    result.Error_code = httpStatusCode;
                    result.Reason = "请求接口不成功";
                    //throw new Exception($"请求接口不成功，状态码：{httpStatusCode}");
                }
            }
            catch (Exception ex)
            {
                result.Reason = ex.Message;
            }

            return result;
        }
    }
}
