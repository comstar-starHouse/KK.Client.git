﻿using Microsoft.Extensions.DependencyInjection;
using System;

namespace KK.EIS.TianYanCha
{
    public static class TianYanChaServiceCollectionExtension
    {
        public static IServiceCollection AddTianYanChaEIS(this
            IServiceCollection services,
            Action<EISOptions> configureAuth = null)
        {
            services.AddTransient<IEISManager, TianyanchaEIS>();
            services.Configure<EISOptions>(configureAuth);
            return services;
        }
    }
}