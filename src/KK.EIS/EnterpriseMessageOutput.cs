﻿using System;
using System.Collections.Generic;
using System.Text;

namespace KK.EIS
{
    public class EnterpriseMessageOutput
    {
        /// <summary>
        /// 企业名
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// 核准日期 为时间戳
        /// </summary>
        public long ApprovedTime { get; set; }

        /// <summary>
        /// 状态
        /// </summary>
        public string RegStatus { get; set; }

        /// <summary>
        /// 纳税人识别号
        /// </summary>
        public string TaxNumber { get; set; }

        /// <summary>
        /// 法定代表人
        /// </summary>
        public string LegalPersonName { get; set; }

        /// <summary>
        /// 注册地
        /// </summary>
        public string RegLocation { get; set; }

        /// <summary>
        /// 经营范围
        /// </summary>
        public string BusinessScope { get; set; }

        /// <summary>
        /// 统一社会信用代码
        /// </summary>
        public string CreditCode { get; set; }

        /// <summary>
        /// 成立日期
        /// </summary>
        public long EstiblishTime { get; set; }

        /// <summary>
        /// 有效期至（当返回值为null时，有效期为 无固定期限）
        /// </summary>
        public long? ToTime { get; set; }

        /// <summary>
        /// 注册资本
        /// </summary>
        public string RegCapital { get; set; }
    }


    public class ResultMessage<T>
    {
        public string Reason { get; set; }
        public int Error_code { get; set; }

        public T Result { get; set; }
    }
}
