﻿using System;

namespace KK.EIS
{
    public interface IEISManager
    {
        ResultMessage<EnterpriseMessageOutput> EnterpriseBasicInformationQueryAsync(string companyMessage);
    }
}
